<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('home');});

Route::get('/home',['as'=>'home','uses'=>'PagesController@trangchu']);
Route::get('newPost',['as'=>'newPost','uses'=>'PagesController@newPost']);
Route::get('mostRead',['as'=>'mostRead','uses'=>'PagesController@mostRead']);
Route::get('fepost/{id}',['as'=>'fepost','uses'=>'PagesController@FePost']);
Route::get('details/{id}',['as'=>'details','uses'=>'PagesController@details']);
Route::get('feimage/{id}',['as'=>'feimage','uses'=>'PagesController@FeImage']);
Route::get('fevideo/{id}',['as'=>'fevideo','uses'=>'PagesController@FeVideo']);




Route::get('upload', ['as'=>'upload', 'uses'=>'quan@upload']);
Route::post('posttest',['posttest','uses'=>'quan@posttest']);


Route::post('postuploads', ['as'=>'postuploads', 'uses'=>'quan@postupload']);
Route::get('/a', function(){
    return view('back-end.category');
});
Route::get('/ab', function(){
    return view('layouts.adminlte');
});

//Route::get('home',['as'=>'newPost','uses'=>'quan@home']);


Route::get('dailyLife',['as'=>'dailyLife','uses'=>'quan@dailyLife']);
Route::get('socialSecurity',['as'=>'socialSecurity','uses'=>'quan@socialSecurity']);
Route::get('humor',['as'=>'humor','uses'=>'quan@humor']);
Route::get('isme',['as'=>'isme','uses'=>'quan@isme']);
Route::get('mediaisme',['as'=>'mediaisme','uses'=>'quan@mediaisme']);
Route::get('mediahumor',['as'=>'mediahumor','uses'=>'quan@mediahumor']);
//Route::get('details',['as'=>'details','uses'=>'quan@details']);




Auth::routes();
Route::get('/homee', 'HomeController@index')->name('homee');


Route::group(['prefix'=>'backend'],function (){

    //done
    Route::group(['prefix'=>'categorygroup'],function (){
        Route::get('category',['as'=>'category','uses'=>'CategoryController@getCategory']);
        Route::get('categoryform',['as'=>'categoryform','uses'=>'CategoryController@getAddCategory']);
        Route::post('categoryforms',['as'=>'categoryforms','uses'=>'CategoryController@postAddCategory']);
        Route::get('geteditc/{id}',['as'=>'geteditc','uses'=>'CategoryController@getEditc']);
        Route::post('posteditc/{id}',['as'=>'posteditc','uses'=>'CategoryController@postEditc']);
        Route::get('getxoac/{id}',['as'=>'getxoac','uses'=>'CategoryController@getXoa']);
    });
    Route::group(['prefix'=>'postgroup'],function (){
        Route::get('post',['as'=>'post','uses'=>'PostController@getPost']);
        Route::get('postform',['as'=>'postform','uses'=>'PostController@getAddPost']);
        Route::post('postforms',['as'=>'postforms','uses'=>'PostController@postAddPost']);
        Route::get('geteditp/{id}',['as'=>'geteditp','uses'=>'PostController@getEditp']);
        Route::post('posteditp/{id}',['as'=>'posteditp','uses'=>'PostController@postEditp']);
        Route::get('getxoap/{id}',['as'=>'getxoap','uses'=>'PostController@getXoap']);
    });
    Route::group(['prefix'=>'imagegroup'],function (){
        Route::get('images',['as'=>'images','uses'=>'ImageController@getImage']);
        Route::get('imageform',['as'=>'imageform','uses'=>'ImageController@getAddImage']);
        Route::post('imageforms',['as'=>'imageforms','uses'=>'ImageController@postAddImage']);
        Route::get('getedits/{id}',['as'=>'getedits','uses'=>'ImageController@getEdits']);
        Route::post('postedits/{id}',['as'=>'postedits','uses'=>'ImageController@postEdits']);
        Route::get('getxoaimg/{id}',['as'=>'getxoaimg','uses'=>'ImageController@getXoaimg']);
//imagecategory
        Route::get('imagecategory',['as'=>'imagecategory','uses'=>'ImageCategoryController@getImageCategory']);
        Route::get('imagecategoryform',['as'=>'imagecategoryform','uses'=>'ImageCategoryController@getAddImageCategory']);
        Route::post('imagecategoryforms',['as'=>'imagecategoryforms','uses'=>'ImageCategoryController@postAddImageCategory']);
        Route::get('getediti/{id}',['as'=>'getediti','uses'=>'ImageCategoryController@getEdit']);
        Route::post('postediti/{id}',['as'=>'postediti','uses'=>'ImageCategoryController@postEdit']);
        Route::get('getxoaa/{id}',['as'=>'getxoaa','uses'=>'ImageCategoryController@getXoaa']);
    });



    Route::group(['prefix'=>'videogroup'],function (){
        Route::get('video',['as'=>'video','uses'=>'VideoController@getVideo']);
        Route::get('videoform',['as'=>'videoform','uses'=>'VideoController@getAddVideo']);
        Route::post('videoforms',['as'=>'videoforms','uses'=>'VideoController@postAddVideo']);
        Route::get('geteditvd/{id}',['as'=>'geteditvd','uses'=>'VideoController@getEditvd']);
        Route::post('posteditvd/{id}',['as'=>'posteditvd','uses'=>'VideoController@postEditvd']);
        Route::get('getxoas/{id}',['as'=>'getxoas','uses'=>'VideoController@getXoas']);





//videocategory
        Route::get('videocategory',['as'=>'videocategory','uses'=>'VideoCategoryController@getVideoCategory']);
        Route::get('videocategoryform',['as'=>'videocategoryform','uses'=>'VideoCategoryController@getAddVideoCategory']);
        Route::post('videocategoryforms',['as'=>'videocategoryforms','uses'=>'VideoCategoryController@postAddVideoCategory']);
        Route::get('geteditv/{id}',['as'=>'geteditv','uses'=>'VideoCategoryController@getEdit']);
        Route::post('posteditv/{id}',['as'=>'posteditv','uses'=>'VideoCategoryController@postEdit']);
        Route::get('getxoa/{id}',['as'=>'getxoa','uses'=>'VideoCategoryController@getXoa']);
    });
    Route::group(['prefix'=>'slidegroup'],function (){
        Route::get('slide',['as'=>'slide','uses'=>'SlideController@getSlide']);
        Route::get('slideform',['as'=>'slideform','uses'=>'SlideController@getAddSlide']);
        Route::post('Slideforms',['as'=>'Slideforms','uses'=>'SlideController@postAddSlide']);
        Route::get('geteditsl/{id}',['as'=>'geteditsl','uses'=>'SlideController@getEditSlide']);
        Route::post('posteditsl/{id}',['as'=>'posteditsl','uses'=>'SlideController@postEditSlide']);
        Route::get('getxoasl/{id}',['as'=>'getxoasl','uses'=>'SlideController@getXoasl']);
    });
});