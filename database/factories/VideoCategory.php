<?php

use Faker\Generator as Faker;

$factory->define(App\Models\VideoCategory::class, function (Faker $faker) {
    return [
        'videocategoryname' => $faker->text(20),
    ];
});