<?php

use Faker\Generator as Faker;

$factory->define(App\Models\ImageCategory::class, function (Faker $faker) {
    return [
        'imagecategoryname' => $faker->text(20),
    ];
});