<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Image::class, function (Faker $faker) {
    return [
        'image' => $faker->text(20),
        'imagename' => $faker->text(20),
        'idimagecategory' => $faker->numberBetween(0,20),
    ];
});