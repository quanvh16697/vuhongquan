<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => bcrypt('abc123'),
        'remember_token' => str_random(10),
    ];
});


//$factory->define(App\Models\Category::class, function (Faker $faker) {
//    return [
//        'category' => $faker->text(20),
//    ];
//});


//$factory->define(App\Models\Image::class, function (Faker $faker) {
//    return [
//        'image' => $faker->text(20),
//        'imagename' => $faker->text(20),
//        'idimagecategory' => $faker->numberBetween(0,1000),
//    ];
//});

//$factory->define(App\Models\ImageCategory::class, function (Faker $faker) {
//    return [
//        'imagecategoryname' => $faker->text(20),
//    ];
//});

//$factory->define(App\Models\Post::class, function (Faker $faker) {
//    return [
//        'content' => $faker->paragraph(1, true),
//        'contentname' => $faker->unique()->safeEmail,
//        'Author' => $faker->name,
//        'thumlbai' => $faker->image(null, 250, 250, null, true, true,null),
//        'idcategory' => $faker->numberBetween(0, 1000),
//    ];
//});

//$factory->define(App\Models\Slide::class, function (Faker $faker) {
//    return [
//        'thumlbai' => $faker->image(null, 250, 250, null, true, true,null),
//        'ten' => $faker->company,
//        'link' => $faker->url,
//    ];
//});

//$factory->define(App\Models\Video::class, function (Faker $faker) {
//    return [
//        'video' => $faker->text(20),
//        'videoname' => $faker->text(20),
//        'idvideocategory' => $faker->numberBetween(0,20),
//    ];
//});

//$factory->define(App\Models\VideoCategory::class, function (Faker $faker) {
//    return [
//        'videocategoryname' => $faker->text(20),
//    ];
//});