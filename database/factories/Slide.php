<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Slide::class, function (Faker $faker) {
    return [
        'thumlbai' => $faker->image(null, 250, 250, null, true, true,null),
        'ten' => $faker->company,
        'link' => $faker->url,
    ];
});
