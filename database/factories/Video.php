<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Video::class, function (Faker $faker) {
    return [
        'video' => $faker->text(20),
        'videoname' => $faker->text(20),
        'idvideocategory' => $faker->numberBetween(0,20),
    ];
});