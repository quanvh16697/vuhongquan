<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Post::class, function (Faker $faker) {
    return [
        'content' => $faker->text,
        'contentname' => $faker->firstName,
        'Author' => $faker->name,
        'thumlbai' => $faker->image(null, 250, 250, null, true, true,null),
        'idcategory' => $faker->numberBetween(0, 1000),
    ];
});