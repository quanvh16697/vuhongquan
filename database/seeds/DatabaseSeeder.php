<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        factory(App\User::class, 5)->create();
//        factory(App\Models\Category::class, 5)->create();
//        factory(App\Models\ImageCategory::class, 5)->create();
//        factory(App\Models\Slide::class, 5)->create();
//        factory(App\Models\Image::class, 5)->create();
//        factory(App\Models\Video::class, 5)->create();
//        factory(App\Models\VideoCategory::class, 5)->create();
//        factory(App\Models\Post::class, 5)->create();


//        DB::table('users')->truncate();
        $this->call(UserSeeder::class);

        $this->call(category::class);

        $this->call(image::class);

        $this->call(imagecategory::class);

        $this->call(post::class);

        $this->call(Slide::class);

        $this->call(video::class);

        $this->call(videocategory::class);



//        factory(\App\User::class)->times(100)->create();
//        $category = factory(Category::class, 5)->create();
//        $image = factory(Image::class, 5)->create();
//        $post = factory(Post::class, 5)->create();
//        $imagecategory = factory(ImageCategory::class, 5)->create();
//        $slide = factory(Slide::class, 5)->create();
//        $video = factory(Video::class, 5)->create();
//        $videocategory = factory(VideoCategory::class, 5)->create();
//         $this->call(category::class);
    }
}
