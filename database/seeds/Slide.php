<?php

use Illuminate\Database\Seeder;

class Slide extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Slide::class, 5)->create();
    }
}
