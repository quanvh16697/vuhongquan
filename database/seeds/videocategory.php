<?php

use Illuminate\Database\Seeder;

class videocategory extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\VideoCategory::class, 5)->create();
    }
}
