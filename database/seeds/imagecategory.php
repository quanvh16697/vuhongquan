<?php

use Illuminate\Database\Seeder;

class imagecategory extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\ImageCategory::class, 5)->create();
    }
}
