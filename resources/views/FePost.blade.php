@extends('master')
@section('NoiDung')
    <div class="">
        <div class="badge badge-info" style="width: 100%;"><h4>{{$category1->category}}</h4></div>
        <div class="row">
            @forelse($posts as $pst)

                <div class="col-md-4">
                    <div class="d-flex flex-column md-3">
                        <div class="p-2"><img src="{{asset('img/'.$pst->thumlbai)}}" style="max-width: 100%" alt="...">
                        </div>
                        <div class="p-2"><a href="{{route('details',[$pst->id])}}">{{$pst->contentname}}</a></div>
                    </div>
                </div>
            @empty
            @endforelse
        </div>
    </div>
    <hr>
    <div class="d-flex justify-content-end" >
        {{ $posts->links() }}
    </div>
@endsection