<div style="margin-bottom: 10px; ">

<div class="bd-example">
    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php $i = 0; ?>
            @foreach($slide as $sl)
                <li data-target="#carouselExampleCaptions" data-slide-to="{{$i}}"
                    @if($i == 0)
                    class="active"
                        @endif
                ></li>
                <?php $i++; ?>
            @endforeach
        </ol>
        <div class="carousel-inner">
            <?php $i = 0; ?>
            @foreach($slide as $sl)
                <div
                        @if($i == 0)
                        class="carousel-item active"
                        @else
                        class="carousel-item "
                        @endif
                >
                    <?php $i++; ?>
                    <a href="{{$sl->link}}"><img src="{{asset('img/'.$sl->thumlbai)}}" class="d-block w-20" alt="..."></a>
                    <div class="carousel-caption d-none d-md-block">
                        <h5>{{$sl->ten}}</h5>
                    </div>
                </div>
            @endforeach

        </div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
</div>
