<nav class="navbar navbar-expand-lg fixed-top navbar-light bg-light" id="nav">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav text-uppercase ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{route('home')}}">Trang chủ</a>
            </li>
            <li class="nav-item">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">Bài viết</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{route('newPost')}}">Mới nhất</a>
                    <!-- <div class="dropdown-divider"></div>đường kẻ ngang menu -->
                    <a class="dropdown-item" href="{{route('mostRead')}}">Đọc nhiều nhất</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">Chủ đề</a>
                <div class="dropdown-menu">
                    @foreach($category as $ctg)
                        @if(count($ctg->post)>0)
                            <a class="dropdown-item" href="{{route('fepost',[$ctg->id])}}">{{$ctg->category}}</a>
                        @endif
                    @endforeach

                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">Hình ảnh</a>
                <div class="dropdown-menu">
                    @foreach($imagecategory as $imgctg)
                        <a class="dropdown-item"
                           href="{{route('feimage',$imgctg->id)}}">{{$imgctg->imagecategoryname}}</a>
                        <!-- <div class="dropdown-divider"></div>đường kẻ ngang menu -->
                    @endforeach
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">Video</a>
                <div class="dropdown-menu">
                    @foreach($videocategory as $vdctg)
                        <a class="dropdown-item" href="{{route('fevideo',$vdctg->id)}}">{{$vdctg->videocategoryname}}</a>
                        <!-- <div class="dropdown-divider"></div>đường kẻ ngang menu -->
                    @endforeach
                </div>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>