@extends('master')
@section('NoiDung')
    <div class="">
        <div class="badge badge-info" style="width: 100%;"><h4>{{$videocategoryy->videocategoryname}}</h4></div>
        <div class="row">
            @forelse($videoo as $vd)
                <div class="col-md-4">
                    <div class="d-flex flex-column md-3">
                        <div class="p-2"><video  width="100%"  controls >
                                <source src="{{asset('img/'.$vd->video)}}" type="video/ogg" />
                            </video>
                        </div>
                        <div class="p-2"><a href="#">{{$vd->videoname}}</a></div>
                    </div>
                </div>
            @empty
            @endforelse
        </div>
    </div>
    <hr>
    <div class="d-flex justify-content-end" >
        {{ $videoo->links() }}
    </div>
@endsection