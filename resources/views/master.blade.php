<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta name="viewport" content="width: device-width, initial-scale:1,">
    <link rel="stylesheet" type="text/css" href="{{asset('bootstrap/css/bootstrap-grid.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('bootstrap/css/bootstrap-reboot.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('bootstrap/css/bootstrap.css')}}">


    <link rel="stylesheet" href="{{asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('adminlte/bower_components/Ionicons/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('adminlte/dist/css/skins/_all-skins.min.css')}}">



    <script type="text/javascript" src="{{asset('bootstrap/js/jquery.js')}}"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="crossorigin="anonymous"></script> -->
    <script type="text/javascript" src="{{asset('bootstrap/js/bootstrap.bundle.js')}}"></script>
    <script type="text/javascript" src="{{asset('bootstrap/js/bootstrap.js')}}"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Import thư viện JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script>
        // kéo xuống khoảng cách 500px thì xuất hiện nút Top-up
        var offset = 500;
        // thời gian di trượt 0.75s ( 1000 = 1s )
        var duration = 750;
        $(function(){
            $(window).scroll(function () {
                if ($(this).scrollTop() > offset)
                    $('#top-up').fadeIn(duration);else
                    $('#top-up').fadeOut(duration);
            });
            $('#top-up').click(function () {
                $('body,html').animate({scrollTop: 0}, duration);
            });
        });
    </script>
    <style type="text/css">
        .nav-item {
            position: relative;
        }
        #top-up {
            background:none;
            font-size: 3em;
            text-shadow:0px 0px 5px #c0c0c0;
            cursor: pointer;
            position: fixed;
            z-index: 9999;
            color:#004993;
            bottom: 20px;
            right: 15px;
            display: none;
        }
    </style>
</head>
<body>
<!-- Tạo menucap2 -->
@include('template.menu')

<div class="container">
@include('template.slidebar')
    <!-- Kết thúc tạo sidebar -->
    <!-- Nội dung -->
    <div class="content" style="background: yellow;">
        <div class="row" style="margin-left: 0px; margin-right: 0px;">
            <div class="col-md-9" style="background: #DDDDDD;">
                <!-- kế thừa nội dung này -->
                @yield('NoiDung')
                {{--                <hr>--}}
                {{--                <nav aria-label="Page navigation example">--}}
                {{--                    <ul class="pagination">--}}
                {{--                        <li class="page-item"><a class="page-link" href="#">Previous</a></li>--}}
                {{--                        <li class="page-item"><a class="page-link" href="#">1</a></li>--}}
                {{--                        <li class="page-item"><a class="page-link" href="#">2</a></li>--}}
                {{--                        <li class="page-item"><a class="page-link" href="#">3</a></li>--}}
                {{--                        <li class="page-item"><a class="page-link" href="#">Next</a></li>--}}
                {{--                    </ul>--}}
                {{--                </nav>--}}






                <!-- kết thúc nội dung -->
            </div>
            <!-- sidebar menu -->
            <div class="col-md-3" style="background: #cccccc;">
                <div class="btn-group" style="width: 100%;">
                    <button type="button" class="btn btn-success">Mới nhất</button>
                    <button type="button" class="btn btn-success">Xem nhiều</button>
                    <button type="button" class="btn btn-success">Bình luận</button>
                </div>
                <hr>
                <div class="d-flex flex-column md-3" style="">
                    <div class="p-2" style="">
                        <img src="https://i-thethao.vnecdn.net/2019/02/22/zUjudx3KhSmonacoatm9-155081236-2653-6427-1550813048_500x300.jpg"
                             alt="..." class="rounded " style="max-width: 100%;">
                    </div>
                    <div class="p-2" style="">
                        <p>b</p>
                    </div>
                </div>
                <hr>
                <div class="d-flex flex-column md-3" style="">
                    <div class="p-2" style="">
                        <img src="https://i-thethao.vnecdn.net/2019/02/22/zUjudx3KhSmonacoatm9-155081236-2653-6427-1550813048_500x300.jpg"
                             alt="..." class="rounded " style="max-width: 100%;">
                    </div>
                    <div class="p-2" style="">
                        <p>b</p>
                    </div>
                </div>
                <hr>
                <div class="d-flex flex-column md-3" style="">
                    <div class="p-2" style="">
                        <img src="https://i-thethao.vnecdn.net/2019/02/22/zUjudx3KhSmonacoatm9-155081236-2653-6427-1550813048_500x300.jpg"
                             alt="..." class="rounded " style="max-width: 100%;">
                    </div>
                    <div class="p-2" style="">
                        <p>b</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- kết thúc nội dung -->
    @include('template.footer')
    <div title="Về đầu trang" onmouseover="this.style.color='#590059'" onmouseout="this.style.color='#004993'" id="top-up">
        <i class="fa fa-caret-square-o-up"></i></div>
</div>
</body>
</html>
