@extends('master')
@section('NoiDung')
    <div class="">
        <div class="badge badge-info" style="width: 100%;"><h4>{{$imagecategoryy->imagecategoryname}}</h4></div>
        <div class="row">
            @forelse($imagee as $img)
                <div class="col-md-4">
                    <div class="d-flex flex-column md-3">
                        <div class="p-2"><img src="{{asset('img/'.$img->image)}}" style="max-width: 100%" alt="...">
                        </div>
                        <div class="p-2"><a href="#">{{$img->imagename}}</a></div>
                    </div>
                </div>
            @empty
            @endforelse
        </div>
    </div>
    <hr>
    <div class="d-flex justify-content-end" >
        {{ $imagee->links() }}
    </div>
@endsection