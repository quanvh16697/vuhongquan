@extends('back-end.master')
@section('tryuikodtyujkuedf')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Data Tables
                <small>advanced tables</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Tables</a></li>
                <li class="active">Data tables</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Post list</h3>
                        </div>
                        @include('thongbao')
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>ID post</th>
                                    <th>Content</th>
                                    <th>Contentname</th>
                                    <th>Auth</th>
                                    <th>Category</th>
                                    <th>Thumlbai</th>
                                    <th></th>
                                </tr>
                                </thead>
                                @foreach($post as $index=>$pst)
                                <tbody>
                                <tr>
                                    <td>{{$index +1}}</td>
                                    <td>{!! $pst->content !!}</td>
                                    <td>{{$pst->contentname}}</td>
                                    <td>{{$pst->Author}}</td>
                                    <td>{{$pst->category->category}}</td>
{{--                                    <td>{{isset($pst->category->category) ? $pst->category->category : 'empty'}}</td>--}}
                                    <td>
                                        <img src="{{asset('img/'.$pst->thumlbai)}}" style="width: 300px;" class="d-block" alt="eror">
{{--    asset('image/{{$post->thumlbai}}')                                    --}}
                                    </td>
{{--                                    route('getedit',[$pst->id])--}}
                                    <td>
                                        <a href="{{route('geteditp',[$pst->id])}}" class="label pull-right bg-blue" ><i class=" fa fa-edit"></i>edit</a>
                                        <a href="{{route('getxoap',[$pst->id])}}" class="label pull-right bg-red" ><i class=" fa fa-trash-o"></i>delete</a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                                </tr>
                                </tfoot>
                            </table>
                            <div style="float: right;">
                                {{ $post->links() }}
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection