@extends('back-end.master')
@section('tryuikodtyujkuedf')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                General Form Elements
                <small>Preview</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">General Elements</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit post</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        @include('error')
                        @include('thongbao')
                        <form role="form" action="{{route('posteditp', [$post->id] )}}" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="box-body">
                                <label for="exampleInputPassword1">Post</label>
                                <div class="form-group">
                                    <select class="form-control" name="txtcategory" id="">
                                        @foreach($category as $ctg)
                                            <option
                                                    @if($post->idcategory == $ctg->id)
                                                    {{"selected"}}
                                                    @endif
                                                    value="{{$ctg->id}}">{{$ctg->category}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Content</label>

                                    {{--                                    <input type="text" name="txtcontent">--}}
                                    <textarea class="textarea" name="txtcontent" placeholder="Place some text here"
                                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$post->content}}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputPassword1">Content_name</label>
                                    <input type="text" value="{{$post->contentname}}" class="form-control"
                                           name="txtcontentname" id="exampleInputPassword1" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Author</label>
                                    <input type="text" value="{{$post->Author}}" class="form-control" name="txtauthor"
                                           id="exampleInputPassword1" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Thumlbai</label>
                                    <div style="border: solid 1px;">
                                        <input type="file" value="{{$post->thumlbai}}" name="imgthumlbai"
                                               enctype="multipart/form-data" id="exampleInputFile">
                                        <p class="help-block">File phải có dạng .png hoặc jpg .</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
                <!--/.col (left) -->
                <!-- right column -->
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection