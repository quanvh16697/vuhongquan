@extends('back-end.master')
@section('tryuikodtyujkuedf')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                General Form Elements
                <small>Preview</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">General Elements</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Slide</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        @include('error')
                        @include('thongbao')
                        <form role="form" action="{{route('posteditsl',[$slide->id])}}" enctype="multipart/form-data" method="post" >
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputFile">Slide</label>
                                    <input type="file" name="imgslide" id="exampleInputFile">

                                    <p class="help-block">upload file có dạng png hoặc jpg</p>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">slide_name</label>
                                    <input type="text" name="txtslidename" value="{{$slide->ten}}" class="form-control" id="exampleInputEmail1" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile"> Link </label>
                                    <input class="form-control" type="text" value="{{$slide->link}}" name="txtlink">
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
                <!--/.col (left) -->
                <!-- right column -->
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection