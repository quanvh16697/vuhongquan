@extends('back-end.master')
@section('tryuikodtyujkuedf')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Data Tables
                <small>advanced tables</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Tables</a></li>
                <li class="active">Data tables</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Images list</h3>
                        </div>
                    @include('thongbao')
                    <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>ID Slide</th>
                                    <th>Slideimage</th>
                                    <th>Slidename</th>
                                    <th>Slidelink</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($slide as $index=>$sl)
                                    <tr>
                                        <td>{{$index+1}}</td>
                                        <td><img src="{{asset('img/'.$sl->thumlbai)}}" style="width: 300px;" class="d-block" alt="eror">
                                        </td>
                                        <td>{{$sl->ten}}</td>
                                        <td>{{$sl->link}}</td>
                                        <td>
                                            <a href="{{route('geteditsl', [$sl->id])}}" class="label pull-right bg-blue"><i class=" fa fa-edit"></i>edit</a>
                                            <a href="{{route('getxoasl', [$sl->id])}}" class="label pull-right bg-red"><i class=" fa fa-trash-o"></i>delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection