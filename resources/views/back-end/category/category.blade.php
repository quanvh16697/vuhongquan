@extends('back-end.master')
@section('tryuikodtyujkuedf')
    <script>
        function myFunction(){
            document.getElementById("click");
            window.confirm('Bạn có đồng ý xóa hay không?');
        }
    </script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Tables
            <small>advanced tables</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div>
                            <h3 class="box-title">List category</h3>
                        </div>
                        <div>
                            span
                        </div>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        @include('thongbao')
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>ID category</th>
                                <th>category </th>
                                <th>F</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($category as $index => $ctg)
                            <tr>
                                <td>{{$index + 1}}</td>
                                <td>{{$ctg->category}}</td>
                                <td> {{-- {{route('getedit', [$ctg->id] )}}  --}}
                                    <a href="{{route('geteditc',[$ctg->id])}}" class="label pull-right bg-blue" ><i class=" fa fa-edit"></i>edit</a>
                                    <a href="{{route('getxoac',[$ctg->id])}}" id="click" onclick="myFunction()" class="label pull-right bg-red" ><i class=" fa fa-trash-o"></i>delete</a>
                                </td>
                            </tr>

                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ID category</th>
                                <th>category</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

@endsection