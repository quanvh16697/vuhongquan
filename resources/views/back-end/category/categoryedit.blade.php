@extends('back-end.master')
@section('tryuikodtyujkuedf')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                General Form Elements
                <small>Preview</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">General Elements</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit category</h3>
                            <smal>{{$category->category}}</smal>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->


                        @include('error')

                        @include('thongbao')

                       <form role="form" action="{{route('posteditc', [$category->id] )}}" method="post"> {{--  --}}
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Edit category</label>
                                    <input type="text" class="form-control" id="category" name="ten" value="{{$category->category}}" placeholder="">
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
                <!--/.col (left) -->
                <!-- right column -->

                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection