@extends('back-end.master')
@section('tryuikodtyujkuedf')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Data Tables
                <small>advanced tables</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Tables</a></li>
                <li class="active">Data tables</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Images list</h3>
                        </div>
                        @include('thongbao')
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>ID Video</th>
                                    <th>Video</th>
                                    <th>Video_Name</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($video as $index => $vd)
                                <tr>
                                    <td>{{$index + 1}}</td>

                                    <td>
                                        <video  width="300" height="200" controls >
                                            <source src="{{asset('img/'.$vd->video)}}" type="video/ogg" />
                                            </video>

{{--                                        <video controls autoplay  width="100px" height="100px"  src="{{asset('img/'.$vd->video)}}"></video>--}}
                                    </td>
                                    <td>{{$vd->videoname}}</td>
                                    <td>{{isset($vd->videocategory->videocategoryname) ? $vd->videocategory->videocategoryname : 'Không có thể loại'}}</td>
                                    <td>
                                        <a href="{{route('geteditvd', [$vd->id])}}" class="label pull-right bg-blue" ><i class=" fa fa-edit"></i>edit</a>
                                        <a href="{{route('getxoas', [$vd->id])}}" class="label pull-right bg-red" ><i class=" fa fa-trash-o"></i>delete</a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID Video</th>
                                    <th>Video</th>
                                    <th>Video_Name</th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection