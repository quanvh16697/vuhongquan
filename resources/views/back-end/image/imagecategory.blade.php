@extends('back-end.master')
@section('tryuikodtyujkuedf')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Data Tables
                <small>advanced tables</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Tables</a></li>
                <li class="active">Data tables</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Image_category list</h3>
                        </div>
                        @include('thongbao')
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>ID_Image_Category_name</th>
                                    <th>Image_Category_name</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($imagecategory as $index => $imgctg)
                                    <tr>
                                        <td>{{$index + 1}}</td>
                                        <td>{{$imgctg->imagecategoryname}}</td>
                                        <td>
                                            <a href="{{route('getediti', [$imgctg->id] )}}" class="label pull-right bg-blue" ><i class=" fa fa-edit"></i>edit</a>
                                            <a href="{{route('getxoaa',[$imgctg->id])}}" class="label pull-right bg-red" ><i class=" fa fa-trash-o"></i>delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID_Image_Category_name</th>
                                    <th>Image_Category_name</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection