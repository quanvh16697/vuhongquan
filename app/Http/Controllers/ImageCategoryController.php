<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\ImageCategory;
use Illuminate\Http\Request;

class ImageCategoryController extends Controller
{
    public function getImageCategory(){
        $imagecategory = ImageCategory::all();
        return view('back-end.image.imagecategory', ['imagecategory'=>$imagecategory]);
    }
    //them
    public function getAddImageCategory()
    {
        return view('back-end.image.imagecategoryform');
    }
    public function postAddImageCategory(Request $request)
    {
        $this->validate($request,
            [
                'ten' => 'required|min:3|max:100'
            ],
            [
                'ten.required' => 'Không được bỏ trống!',
                'ten.min' => 'Tên phải quá 3 ký tự',
                'ten.max' => 'Tên không được quá 100 ký tự'
            ]
        );
        $imagecategory = new ImageCategory;//tạo mới
        $imagecategory->imagecategoryname = $request->ten;//Them vao bang
        $imagecategory->save();
        return redirect()->route('imagecategoryform')->with('thongbao', 'Thêm thể loại thành công');
    }
    //Sua
    public function getEdit($id)
    {
        $imagecategory = ImageCategory::find($id); //tim id
        return view('back-end.image.imagecategoryedit', ['imagecategory' => $imagecategory]);
    }
    public function postEdit(Request $request, $id)
    {
        $imagecategory = ImageCategory::find($id);
        $this->validate($request, [//bat loi validate
            'ten' => 'required|unique:imagecategory,imagecategoryname|min:3|max:100'
            //required: Không được để trống. unique: tenbang tencot: không được trùng. min.max
        ],
            [
                'ten.required' => 'Không được để chống.',
                'ten.unique' => 'Tên đã tồn tại',
                'ten.min' => 'Không được quá 100 ký tự và ít nhất 3 ký tự',
                'ten.max' => 'Không được quá 100 ký tự và ít nhất 3 ký tự'
            ]
        );
//        dd($request);
        $imagecategory->imagecategoryname = $request->ten;//ham sua ten trong bang
        $imagecategory->save();//ham luu lai
        return redirect()->route('getediti', [$imagecategory->id])->with('thongbao', 'Sửa thể loại thành công');
    }
//xoa
    public function getXoaa($id){
        $imagecategory = ImageCategory::find($id);
        $imagecategory -> delete();

        return redirect()->route('imagecategory')->with('thongbao','Xóa thành công');
    }
}
