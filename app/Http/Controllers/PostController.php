<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function getPost()
    {
        $post = Post::all();
        $post = Post::paginate(5);
        $category = Category::all();//lấy ra bảng category
        return view('back-end.post.post', compact('post', 'category'));
    }

//them
    public function getAddPost()
    {
        $category = Category::all();//lấy ra bảng category
        return view('back-end.post.postform', compact('category'));// Truyền biến sang bên postforms
    }

    public function postAddPost(Request $request)
    {
        //dd(all($request));

        $this->validate($request,
            [
                'txtcontent' => 'required|unique:post,content|min:3|max:3000',
                'txtcontentname' => 'required|unique:post,content|min:3|max:100',
                'txtauthor' => 'required|min:3|max:20',
                'txtcategory' => 'required'
                //            'imgthumlbai'=>'image'
            ],
            [
                'txtcontent.required' => 'Bạn chưa viết nội dung',
                'txtcontent.min' => 'Tối thiểu 3 ký tự',
                'txtcontent.max' => 'Tối đa 3000 ký tự',
                'txtcontent.unique' => 'Nội dung đã tồn tại',


                'txtcontentname.required' => 'Không được bỏ chống tên tin',
                'txtcontentname.min' => 'Tối thiểu 3 ký tự',
                'txtcontentname.max' => 'Tối đa 100 ký tự',
                'txtcontentname.unique' => 'Tên đã tồn tại',


                'txtauthor.required' => 'Không được bỏ chống tên tác giả',
                'txtauthor.min' => 'Tối thiểu 3 ký tự',
                'txtauthor.max' => 'Tối đa 20 ký tự',
                'txtcategory.required' => 'bạn chưa chọn category'
                //'imgthumlbai.image'=>'File không hợp lệ'
            ]
        );
        $post = new Post;
        $post->content = $request->txtcontent;
        $post->contentname = $request->txtcontentname;
        $post->Author = $request->txtauthor;
        $post->idcategory = $request->txtcategory;
        if ($request->hasFile('imgthumlbai')) {
            $file = $request->file('imgthumlbai');
            if ($file->getClientOriginalExtension('imgthumlbai') == "jpg") {
                $name = $file->getClientOriginalName('imgthumlbai');
                $file->move("img", $name);
                $post->thumlbai = $name;
//                return redirect()->route('postform')->with('thongbao','upload thanh cong');
            } else {
                return redirect()->route('postform')->with('thongbao', 'File khong hop le');
            }
        } else {
            return redirect()->route('postform')->with('thongbao', 'Chưa có file ảnh');
        }
        $post->save();
        return redirect()->route('postform')->with('thongbao', 'Thêm thành công');
    }


//Sua
    public function getEditp($id)
    {
        $category = Category::all();
        $post = Post::find($id); //tim id
        return view('back-end.post.postedit', compact('post', 'category'));
    }

    public function postEditp(Request $request, $id)
    {
        $post = Post::find($id);
        $this->validate($request,
            [
                'txtcontent' => 'required|unique:post,content|min:3|max:3000',
                'txtcontentname' => 'required|unique:post,content|min:3|max:100',
                'txtauthor' => 'required|min:3|max:20',
                'txtcategory' => 'required'
                //            'imgthumlbai'=>'image'
            ],
            [
                'txtcontent.required' => 'Bạn chưa viết nội dung',
                'txtcontent.min' => 'Tối thiểu 3 ký tự',
                'txtcontent.max' => 'Tối đa 3000 ký tự',
                'txtcontent.unique' => 'Nội dung đã tồn tại',


                'txtcontentname.required' => 'Không được bỏ chống tên tin',
                'txtcontentname.min' => 'Tối thiểu 3 ký tự',
                'txtcontentname.max' => 'Tối đa 100 ký tự',
                'txtcontentname.unique' => 'Tên đã tồn tại',


                'txtauthor.required' => 'Không được bỏ chống tên tác giả',
                'txtauthor.min' => 'Tối thiểu 3 ký tự',
                'txtauthor.max' => 'Tối đa 20 ký tự',
                'txtcategory.required' => 'bạn chưa chọn category'
                //'imgthumlbai.image'=>'File không hợp lệ'
            ]
        );
        //dd($request);
        $post->content = $request->txtcontent;
        $post->contentname = $request->txtcontentname;
        $post->Author = $request->txtauthor;
        $post->idcategory = $request->txtcategory;
        if ($request->hasFile('imgthumlbai')) {
            $file = $request->file('imgthumlbai');
            if ($file->getClientOriginalExtension('imgthumlbai') == "jpg") {
                $name = $file->getClientOriginalName('imgthumlbai');
                $file->move("img", $name);
                $post->thumlbai = $name;
            } else {
                return redirect()->route('geteditp', [$post->id])->with('thongbao', 'File Không hợp lệ');
            }
        } else {
            return redirect()->route('geteditp', [$post->id])->with('thongbao', 'Chưa có file ảnh');
        }
        $post->save();
        return redirect()->route('geteditp', [$post->id])->with('thongbao', 'Sửa bài viết thành công');
    }


//Xoa
    public function getXoap($id)
    {
        $post = Post::find($id);
        $post->delete();

        return redirect()->route('post')->with('thongbao', 'Xóa thành công');
    }

}






