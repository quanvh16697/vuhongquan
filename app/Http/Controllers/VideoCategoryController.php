<?php

namespace App\Http\Controllers;

use App\Models\VideoCategory;
use Illuminate\Http\Request;

class VideoCategoryController extends Controller
{
    public function getVideoCategory(){
        $videocategory = VideoCategory::all();
        return view('back-end.video.videocategory', ['videocategory'=>$videocategory]);
    }
//them
    public function getAddVideoCategory()
    {
        return view('back-end.video.videocategoryform');
    }

    public function postAddVideoCategory(Request $request)
    {
        $this->validate($request,
            [
                'ten' => 'required|min:3|max:100'
            ],
            [
                'ten.required' => 'Không được bỏ trống!',
                'ten.min' => 'Tên phải quá 3 ký tự',
                'ten.max' => 'Tên không được quá 100 ký tự'
            ]
        );
        $videocategory = new VideoCategory;//tạo mới
        $videocategory->videocategoryname = $request->ten;//Them vao bang
        $videocategory->save();
        return redirect()->route('videocategoryform')->with('thongbao', 'Thêm thể loại thành công');
    }

    //Sua
    public function getEdit($id)
    {
        $videocategory = VideoCategory::find($id); //tim id
        return view('back-end.video.videocategoryedit', ['videocategory' => $videocategory]);
    }
    public function postEdit(Request $request, $id)
    {
        $videocategory = VideoCategory::find($id);
        $this->validate($request, [//bat loi validate
            'ten' => 'required|unique:videocategory,videocategoryname|min:3|max:100'
            //required: Không được để trống. unique: tenbang tencot: không được trùng. min.max
        ],
            [
                'ten.required' => 'Không được để chống.',
                'ten.unique' => 'Tên đã tồn tại',
                'ten.min' => 'Không được quá 100 ký tự và ít nhất 3 ký tự',
                'ten.max' => 'Không được quá 100 ký tự và ít nhất 3 ký tự'
            ]
        );
//        dd($request);
        $videocategory->videocategoryname = $request->ten;//ham sua ten trong bang
        $videocategory->save();//ham luu lai
        return redirect()->route('geteditv', [$videocategory->id])->with('thongbao', 'Sửa thể loại thành công');
    }



    public function getXoa($id){
        $videocategory = VideoCategory::find($id);
        $videocategory -> delete();
        return redirect()->route('videocategory')->with('thongbao','Xóa thành công');
    }
}
