<?php

namespace App\Http\Controllers;

use App\Models\Slide;
use Illuminate\Http\Request;

class SlideController extends Controller
{
    public  function getSlide(){
        $slide = Slide::all();
        return view('back-end.slidebar.slide', compact('slide'));
    }

    //them
    public function getAddSlide()
    {
        $slide = Slide::all();//lấy ra bảng category
        return view('back-end.slidebar.slideform', compact('slide'));// Truyền biến sang bên postforms
    }

    public function postAddSlide(Request $request)
    {
//        dd($request);

        $this->validate($request,
            [
                'txtslidename' => 'required|min:3|max:100',
                'txtlink' => 'required|min:3|max:100',
            ],
            [
                'txtslidename.required' => 'Không được bỏ chống tên Slide',
                'txtslidename.min' => 'Tối thiểu 3 ký tự',
                'txtslidename.max' => 'Tối đa 100 ký tự',

                'txtlink.required' => 'Không được bỏ chống link',
                'txtlink.min' => 'Tối thiểu 3 ký tự',
                'txtlink.max' => 'Tối đa 100 ký tự',
            ]
        );
        $slide = new Slide;
        $slide->ten = $request->txtslidename;
        $slide->link = $request->txtlink;

        if($request->hasFile('imgslide')){
            $file = $request->file('imgslide');
            if ($file->getClientOriginalExtension('imgslide') == "jpg"){
                $name = $file->getClientOriginalName('imgslide');
                $file->move("img",$name);
                $slide->thumlbai = $name;
            }
            else{
                return redirect()->route('slideform')->with('thongbao', 'File không hợp lệ');
            }
        }
        else{
            return redirect()->route('slideform')->with('thongbao', 'Không được để chống file');
        }
        $slide->save();
        return redirect()->route('slideform')->with('thongbao', 'Thêm Slide thành công');
    }

//geteditvd
////Sua
    public function getEditSlide($id){
        $slide = Slide::find($id); //tim id
        return view('back-end.slidebar.slideedit',compact('slide'));
    }
    public function postEditSlide(Request $request,$id){
        $slide = Slide::find($id);
        $this->validate($request,
            [
                'txtslidename' => 'required|min:3|max:100',
                'txtlink' => 'required|min:3|max:100',
            ],
            [
                'txtslidename.required' => 'Không được bỏ chống tên Slide',
                'txtslidename.min' => 'Tối thiểu 3 ký tự',
                'txtslidename.max' => 'Tối đa 100 ký tự',

                'txtlink.required' => 'Không được bỏ chống link',
                'txtlink.min' => 'Tối thiểu 3 ký tự',
                'txtlink.max' => 'Tối đa 100 ký tự',
            ]
        );
        //dd($request);
        $slide->ten = $request->txtslidename;
        $slide->link = $request->txtlink;

        if($request->hasFile('imgslide')){
            $file = $request->file('imgslide');
            if ($file->getClientOriginalExtension('imgslide') == "jpg"){
                $name = $file->getClientOriginalName('imgslide');
                $file->move("img",$name);
                $slide->thumlbai = $name;
            }
            else{
                return redirect()->route('geteditsl',[$slide->id])->with('thongbao', 'File không hợp lệ');
            }
        }
        else{
            return redirect()->route('geteditsl',[$slide->id])->with('thongbao', 'Không được để chống file');
        }
        $slide->save();
        return redirect()->route('geteditsl',[$slide->id])->with('thongbao', 'Sửa Slide thành công');
    }



    public function getXoasl($id){
        $slide = Slide::find($id);
        $slide -> delete();

        return redirect()->route('slide')->with('thongbao','Xóa thành công');
    }
}



