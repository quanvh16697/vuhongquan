<?php

namespace App\Http\Controllers;

use App\Models\Video;
use App\Models\VideoCategory;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    public function getVideo(){
        $video = Video::all();
        return view('back-end.video.video', ['video'=>$video]);
    }




    //them
    public function getAddVideo()
    {
        $videocategory = VideoCategory::all();//lấy ra bảng category
        return view('back-end.video.videoform', compact('videocategory'));// Truyền biến sang bên postforms
    }

    public function postAddVideo(Request $request)
    {
        $this->validate($request,
            [
                'txtvideoname' => 'required|unique:video,videoname|min:3|max:100',
            ],
            [
                'txtvideoname.required' => 'Không được bỏ chống tên ảnh',
                'txtvideoname.min' => 'Tối thiểu 3 ký tự',
                'txtvideoname.max' => 'Tối đa 100 ký tự',
                'txtvideoname.unique' => 'Tên đã tồn tại',
            ]
        );
        $video = new Video;
        $video->videoname = $request->txtvideoname;
        $video->idvideocategory = $request->txtvideocategory;
        if($request->hasFile('video')){
            $file = $request->file('video');
            if ($file->getClientOriginalExtension('video') == "MP4"||$file->getClientOriginalExtension('video') == "mp4"){
                $name = $file->getClientOriginalName('video');
                $file->move("img",$name);
                $video->video = $name;
            }
            else{
                return redirect()->route('videoform')->with('thongbao', 'File không hợp lệ');
            }
        }
        else{
            return redirect()->route('videoform')->with('thongbao', 'Không được để chống file');
        }





        $video->save();
//        dd('Them thanh cong');
        return redirect()->route('videoform')->with('thongbao', 'Thêm video thành công');
    }

//geteditvd
//Sua
    public function getEditvd($id){
        $videocategory = VideoCategory::all();
        $video = Video::find($id); //tim id
        return view('back-end.video.videoedit',compact('video','videocategory'));
    }
    public function postEditvd(Request $request,$id){
        $video = Video::find($id);
        $this->validate($request,
            [
                'txtvideoname' => 'required|unique:video,videoname|min:3|max:100',
            ],
            [
                'txtvideoname.required' => 'Không được bỏ chống tên Video',
                'txtvideoname.min' => 'Tối thiểu 3 ký tự',
                'txtvideoname.max' => 'Tối đa 100 ký tự',
                'txtvideoname.unique' => 'Tên đã tồn tại',
            ]
        );
        //dd($request);
        $video->video = $request->video;
        $video->videoname = $request->txtvideoname;
        $video->idvideocategory = $request->txtvideocategory;
        $video->save();
        return redirect()->route('getedits', [$video->id])->with('thongbao', 'Sửa thành công');
    }



    public function getXoas($id){
        $video = Video::find($id);
        $video -> delete();

        return redirect()->route('video')->with('thongbao','Xóa thành công');
    }
}
