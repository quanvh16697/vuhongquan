<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Image;
use App\Models\ImageCategory;
use App\Models\Post;
use App\Models\Slide;
use App\Models\Video;
use App\Models\VideoCategory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class PagesController extends Controller
{
    function __construct()//Hàm này để share cái các biến để đỡ phải viết đi viết lại ở trên một function khác
    {
        $category = Category::all();
        $videocategory = VideoCategory::all();
        $imagecategory = ImageCategory::all();
        $slide = Slide::all();

        view()->share(compact('category', 'videocategory', 'imagecategory','slide'));
    }

    public function trangchu(){
        return View('home',compact('category', 'videocategory', 'imagecategory', 'slide'));
    }
    public function newPost(){

        return View('NewPost',compact('category', 'videocategory', 'imagecategory'));
    }
    public function mostRead(){

        return View('MostRead',compact('category', 'videocategory', 'imagecategory'));
    }
    public function FePost($id){
        $category1 = Category::find($id);
        $posts = Post::where('idcategory', $id)->paginate(6);
        return View( 'FePost', compact('posts', 'category1'));
    }
    public function details($id){
        $category1 = Category::find($id);
        $post2 = Post::find($id);
        $posts3 = Post::where('idcategory', $post2->idcategory)->take(4)->get();
        return view('details',compact('category1', 'post2', 'posts3'));

    }
    public function FeImage($id){
        $imagecategoryy = ImageCategory::find($id);

        $imagee = Image::where('idimagecategory', $id)->paginate(6);
        return View( 'FeImage', compact('imagee', 'imagecategoryy'));
    }
    public function FeVideo($id){
        $videocategoryy = VideoCategory::find($id);

        $videoo = Video::where('idvideocategory', $id)->paginate(6);
        return View( 'FeVideo', compact('videoo', 'videocategoryy'));
    }

}
