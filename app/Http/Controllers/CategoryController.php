<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class   CategoryController extends Controller
{
    public function getCategory()
    {
        $category = Category::all();
        return view('back-end.category.category', ['category' => $category]);
    }

    //Them

    public function getAddCategory()
    {
        return view('back-end.category.categoryform');
    }

    public function postAddCategory(Request $request)
    {
        $this->validate($request,
            [
                'ten' => 'required|min:3|max:100'
            ],
            [
                'ten.required' => 'Không được bỏ trống!',
                'ten.min' => 'Tên phải quá 3 ký tự',
                'ten.max' => 'Tên không được quá 100 ký tự'
            ]
        );
        $category = new Category;//tạo mới
        $category->category = $request->ten;//Them vao bang
        $category->save();
        return redirect()->route('categoryform')->with('thongbao', 'Thêm thể loại thành công');
    }

    //Sua
    public function getEditc($id)
    {
        $category = Category::find($id); //tim id
        return view('back-end.category.categoryedit', ['category' => $category]);
    }

    public function postEditc(Request $request, $id)
    {
        $category = Category::find($id);
        $this->validate($request, [//bat loi validate
            'ten' => 'required|unique:category,category|min:3|max:100'
            //required: Không được để trống. unique: tenbang tencot: không được trùng. min.max
        ],
            [
                'ten.required' => 'Không được để chống.',
                'ten.unique' => 'Tên đã tồn tại',
                'ten.min' => 'Không được quá 100 ký tự và ít nhất 3 ký tự',
                'ten.max' => 'Không được quá 100 ký tự và ít nhất 3 ký tự'
            ]
        );
//        dd($request);
        $category->category = $request->ten;//ham sua ten trong bang
        $category->save();//ham luu lai
        return redirect()->route('geteditc', [$category->id])->with('thongbao', 'Sửa thể loại thành công');
    }


//Xoa
    public function getXoa($id)
    {
        $category = Category::find($id);
        $category->delete();

        return redirect()->route('category')->with('thongbao', 'Xóa thành công');
    }
}
