<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\ImageCategory;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function getImage(){
        $image = Image::all();
        return view('back-end.image.images',['image'=>$image]);
    }



    //them
    public function getAddImage()
    {
        $imagecategory = ImageCategory::all();//lấy ra bảng category
        return view('back-end.image.imageform', compact('imagecategory'));// Truyền biến sang bên postforms
    }

    public function postAddImage(Request $request)
    {
//        dd($request);

        $this->validate($request,
            [
                'txtimagename' => 'required|unique:image,imagename|min:3|max:100',
            ],
            [
                'txtimagename.required' => 'Không được bỏ chống tên ảnh',
                'txtimagename.min' => 'Tối thiểu 3 ký tự',
                'txtimagename.max' => 'Tối đa 100 ký tự',
                'txtimagename.unique' => 'Tên đã tồn tại',
            ]
        );

        $image = new Image;
        $image->imagename = $request->txtimagename;
        $image->idimagecategory = $request->txtimagecategory;
        if($request->hasFile('image')){
            $file = $request->file('image');
            if ($file->getClientOriginalExtension('image') == "jpg"||$file->getClientOriginalExtension('image') == "png"){
                $name = $file->getClientOriginalName('image');
                $file->move("img",$name);
                $image->image = $name;
            }
            else{
                return redirect()->route('imageform')->with('thongbao', 'File không hợp lệ');
            }
        }
        else{
            dd('jksahdsjhfjsg');
//            return redirect()->route('imageform')->with('thongbao', 'Không được để chống file');
        }
        $image->save();
//        dd('Them thanh cong');
        return redirect()->route('imageform')->with('thongbao', 'Thêm ảnh thành công');
    }


//Sua
    public function getEdits($id){
        $imagecategory = ImageCategory::all();
        $image = Image::find($id); //tim id
        return view('back-end.image.imageedit',compact('image','imagecategory'));
    }
    public function postEdits(Request $request,$id){
        $image = Image::find($id);
        $this->validate($request,
            [
                'txtimagename' => 'required|unique:image,imagename|min:3|max:100',
            ],
            [
                'txtimagename.required' => 'Không được bỏ chống tên ảnh',
                'txtimagename.min' => 'Tối thiểu 3 ký tự',
                'txtimagename.max' => 'Tối đa 100 ký tự',
                'txtimagename.unique' => 'Tên đã tồn tại',
            ]
        );
        //dd($request);
        $image->image = $request->image;
        $image->imagename = $request->txtimagename;
        $image->idimagecategory = $request->txtimagecategory;
        $image->save();
        return redirect()->route('getedits', [$image->id])->with('thongbao', 'Sửa thành công');
    }
    public function getXoaimg($id){
        $image = Image::find($id);
        $image -> delete();

        return redirect()->route('images')->with('thongbao','Xóa thành công');
    }
}
