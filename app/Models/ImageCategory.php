<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImageCategory extends Model
{
    protected $table = 'imagecategory';
    public function image(){
        return $this->hasMany('App\Models\ImageCategory', 'idimagecategory', 'id');
    }
}
