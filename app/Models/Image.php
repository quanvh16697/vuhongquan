<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'image';
    public function imagecategory(){
        return $this->belongsTo('App\Models\ImageCategory', 'idimagecategory', 'id');
    }
}
