<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table = 'video';
    public function videocategory(){
        return $this->belongsTo('App\Models\VideoCategory', 'idvideocategory', 'id');
    }
}
