<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'post';
    protected $fillable = ['content', 'contentname', 'Author', 'idcategory', 'thumlbai'];
//    public function category(){
//        return $this->belongsTo('App\Models\Category', 'idcategory','id');
//    }
//
//    public function get(){
//        dd(123);
//    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'idcategory', 'id');
    }
}
