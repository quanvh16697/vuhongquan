<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoCategory extends Model
{
    protected $table = 'videocategory';
    public function video(){
        return $this->hasMany('App\Models\Video', 'idvideocategory', 'id');
    }
}
